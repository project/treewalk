<?php


class treewalk_HierarchyFactory {

  function taxonomyTermReference($fieldname, $entityType, $depthFirst = FALSE) {

    // Determine the vocabulary
    $fieldinfo = field_info_field($fieldname);
    if (isset($fieldinfo['settings']['allowed_values'][0]['vocabulary'])) {
      $vocabulary = taxonomy_vocabulary_load($fieldinfo['settings']['allowed_values'][0]['vocabulary']);
    }
    else {
      $vocabulary = NULL;
    }

    // Create parent hierarchy based on taxonomy term parent/child
    $parentHierarchy = $this->taxonomyTerm($vocabulary, $depthFirst);

    // Create entity hierarchy based on entity -> term reference.
    $parentChildModel = new treewalk_ParentChildModel_TaxonomyTermReference($fieldname, $entityType);
    $hierarchy = new treewalk_Hierarchy_ExternalParentChild($parentHierarchy, $parentChildModel);

    return $hierarchy;
  }

  function taxonomyTerm($vocabulary = NULL, $depthFirst = FALSE) {
    $parentChildModel = new treewalk_ParentChildModel_TaxonomyTerm($vocabulary);
    $hierarchy = new treewalk_Hierarchy_InternalParentChild($parentChildModel, $depthFirst);
    return $hierarchy;
  }

  function internalEntityReference($fieldname, $entityType, $depthFirst = FALSE) {
    $parentChildModel = new treewalk_ParentChildModel_EntityReference($fieldname, $entityType);
    $hierarchy = new treewalk_Hierarchy_InternalParentChild($parentChildModel, $depthFirst);
  }
}
